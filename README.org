#+TITLE: Shiny Mammalian Cerebellum App
#+AUTHOR: Kevin Leiss

* Description
Shiny app to explore the development and evolution of the mammalian cerebellum in three species (human, mouse and opossum) on single cell level.

* Reference
Sepp, Mari; Leiss, Kevin; Sarropoulos, Ioannis; Murat, Florent; Okonechnikov, Konstantin; Joshi, Piyush; Leushkin, Evgeny; Mbengue, Noe; Schneider, Céline; Schmidt, Julia; Trost, Nils; Spänig, Lisa; Giere, Peter; Khaitovich, Philipp; Lisgo, Steven; Palkovits, Miklós; Kutscher, Lena M.; Anders, Simon; Cardoso-Moreira, Margarida; Pfister, Stefan M.; Kaessmann, Henrik, 2021, **"Cellular development and evolution of the mammalian cerebellum"** [submitted]

* Session Info
#+begin_example
R version 3.6.3 (2020-02-29)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 18.04.6 LTS

Matrix products: default
BLAS:   /usr/lib/x86_64-linux-gnu/openblas/libblas.so.3
LAPACK: /usr/lib/x86_64-linux-gnu/libopenblasp-r0.2.20.so

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C               LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8     LC_MONETARY=en_US.UTF-8
 [6] LC_MESSAGES=en_US.UTF-8    LC_PAPER=en_US.UTF-8       LC_NAME=C                  LC_ADDRESS=C               LC_TELEPHONE=C
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C

attached base packages:
[1] parallel  stats4    stats     graphics  grDevices utils     datasets  methods   base

other attached packages:
 [1] plotly_4.9.0                shinycssloaders_0.2.0       shiny_1.4.0                 SingleCellExperiment_1.6.0  SummarizedExperiment_1.14.0
 [6] DelayedArray_0.10.0         BiocParallel_1.18.0         matrixStats_0.56.0          Biobase_2.44.0              GenomicRanges_1.36.0
[11] GenomeInfoDb_1.20.0         IRanges_2.18.0              S4Vectors_0.22.0            BiocGenerics_0.30.0         forcats_0.4.0
[16] stringr_1.4.0               dplyr_0.8.3                 purrr_0.3.3                 readr_1.3.1                 tidyr_1.0.0
[21] tibble_2.1.3                ggplot2_3.3.2               tidyverse_1.3.0

loaded via a namespace (and not attached):
 [1] nlme_3.1-152           bitops_1.0-6           fs_1.3.1               lubridate_1.7.4        httr_1.4.2             tools_3.6.3
 [7] backports_1.1.4        utf8_1.1.4             R6_2.4.1               DBI_1.1.1              lazyeval_0.2.2         colorspace_1.4-1
[13] withr_2.4.1            tidyselect_0.2.5       gridExtra_2.3          compiler_3.6.3         cli_2.3.1              rvest_1.0.0
[19] Cairo_1.5-10           xml2_1.3.2             labeling_0.3           scales_1.1.1           digest_0.6.18          XVector_0.24.0
[25] pkgconfig_2.0.3        htmltools_0.4.0        dbplyr_1.4.2           fastmap_1.0.1          htmlwidgets_1.3        rlang_0.4.10
[31] readxl_1.3.1           rstudioapi_0.13        generics_0.0.2         farver_2.0.3           jsonlite_1.7.2         crosstalk_1.0.0
[37] RCurl_1.98-1.2         magrittr_2.0.1         GenomeInfoDbData_1.2.1 Matrix_1.3-4           Rcpp_1.0.5             munsell_0.5.0
[43] fansi_0.4.0            viridis_0.5.1          lifecycle_1.0.0        yaml_2.2.0             stringi_1.4.3          zlibbioc_1.30.0
[49] grid_3.6.3             promises_1.1.1         crayon_1.4.1           lattice_0.20-45        haven_2.3.1            hms_1.0.0
[55] pillar_1.4.3           reprex_0.3.0           glue_1.4.1.9000        data.table_1.12.2      modelr_0.1.5           vctrs_0.3.6
[61] httpuv_1.5.2           cellranger_1.1.0       gtable_0.3.0           assertthat_0.2.1       mime_0.10              xtable_1.8-4
[67] broom_0.5.2            later_1.1.0.1          viridisLite_0.3.0      ellipsis_0.1.0
#+end_example


* Licence
This work is licenced under creative commons: [[https://creativecommons.org/licenses/by/4.0/][CC BY 4.0]]
