#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

# SETUP ------------------------------------------------------------------------
library(shiny)
library(tidyverse)
library(SingleCellExperiment)
library(shinycssloaders)
library(plotly)

theme_set(theme_classic())

## FUNCTIONS -------------------------------------------------------------------
layout_ggplotly <- function(gg, x = -.05, y = -.070, x_legend=1.05, y_legend=0.95, mar=list(l=50, r=150)){
    # The 1 and 2 goes into the list that contains the options for the x and y axis labels respectively
    gg[['x']][['layout']][['annotations']][[1]][['y']] <- x
    gg[['x']][['layout']][['annotations']][[2]][['x']] <- y
    # gg[['x']][['layout']][['annotations']][[11]][['x']] <- x_legend # the legend title was the 11-th list element in my case!
    gg[['x']][['layout']][['legend']][['y']] <- y_legend
    gg[['x']][['layout']][['legend']][['x']] <- x_legend
    gg %>% layout(margin = mar)
}

## Loading static data ---------------------------------------------------------
scl <- readRDS('data/all_sc_data.rds') 

o2o.pb.exprs <- read_csv('data/bulk_data/o2o_pseudobulks.csv') %>% 
    mutate(species = recode(species,
                            HUM = 'Human',
                            MOU = 'Mouse',
                            OPO = 'Opossum'))

o2o.info <- read_csv('data/metadata/o2o2o_gene_matching.csv')

umap.color.by.choices <- list(
    'Cell type' = 'cell_type',
    'Gene expression' = 'exprs',
    'Cell state' = 'dev_state',
    'Cell subtype' = 'subtype',
    'Biol. replicate' = 'TissueID',
    'Developmental stage' = 'Stage',
    'Chromium version' = 'Capture.System',
    'Chromium run' = 'batch'
)

color_df <- read_csv('data/metadata/color_codes_annotation.csv')

# cell type colors
tmp <- color_df %>% 
    select(cell_type, cell_type_colour) %>% 
    distinct() %>% 
    drop_na() 
color.ct <- tmp$cell_type_colour
names(color.ct) <- tmp$cell_type

# dev_state colors
tmp <- color_df %>% 
    select(dev_state, dev_state_colour) %>% 
    distinct() %>% 
    drop_na()
color.ds <- tmp$dev_state_colour
names(color.ds) <- tmp$dev_state

# subtype colors
tmp <- color_df %>% 
    select(subtype, subtype_colour) %>% 
    distinct() %>% 
    drop_na()
color.st <- tmp$subtype_colour
names(color.st) <- tmp$subtype

# stage colors
tmp <- color_df %>% 
    select(- `HUM corr. stage`) %>% 
    select(ends_with('Stage'), Stage_colour) %>% 
    gather('sp', 'Stage', ends_with('Stage')) %>% 
    mutate(species = toupper(str_extract(sp,'^...'))) %>% 
    select(-sp) %>% 
    drop_na() %>% 
    unite(Stage, species, Stage)

color.stage <- tmp$Stage_colour
names(color.stage) <- tmp$Stage


color.list <- list(
    dev_state = color.ds,
    cell_type = color.ct,
    subtype = color.st,
    Stage = color.stage
)

# UI ---------------------------------------------------------------------------
ui <- fluidPage(
    
    # TITLE
    titlePanel("Single Nucleus RNA-Seq of the Mammalian Cerebellum"),
    p('Explore the evolution and development of the mammalian cerebellum, by ', 
      a('Kaessmann Lab', href = 'https://www.zmbh.uni-heidelberg.de/Kaessmann/'),
      ', v0.1 (beta)'),
    
    ## Tab page ----------------------------------------------------------------
    tabsetPanel(
        
        ### per species gene expression ------------------------------------------
        tabPanel(title = 'Explore a single species',
                 br(),
                 # Sidebar 
                 sidebarLayout(
                     sidebarPanel(
                         p('Note: After these settings are updated, please click "Apply"'),
                         selectInput('in.species.to.use',
                                     label = 'Select a species:',
                                     choices = list('Human' = 'HUM', 
                                                    'Mouse' = 'MOU', 
                                                    'Opossum' = 'OPO')),
                         
                         # dropdown menu with attributes to color
                         # the UMAP
                         selectInput('in.color.by',
                                     label = 'Color UMAP by:',
                                     choices = umap.color.by.choices),
                         
                         # if gene expression should be plotted
                         # uiOutput('in.single.gene.ui'),
                         textInput('in.single.gene',
                                   label = 'Gene to plot [ENSEMB ID, symbol]',
                                   value = 'Ca8'),
                         # uiOutput('in.sinlge.gene.log.ui'),
                         
                         radioButtons('in.count.to.use',
                                      label = 'UMI counting mode:',
                                      choices = c('pre-mRNA', 'exonic')),
                         
                         # updates the plot
                         submitButton(text = 'Apply', icon = icon('arrows-rotate')),
                         br(),
                         p("All plots are interactive! You can zoom, select different values from the legend
                            and get annotations, when hovering your mouse over the cells. This is especially handy
                            if you plot a gene's expression."),
                         width = '3'
                     ),
                     
                     # Main content
                     mainPanel(
                         tabsetPanel(
                             tabPanel(
                                 'UMAP explorer',
                                 br(),
                                 plotlyOutput('out.ssp.umap.overview', 
                                              height = '600px',
                                              width = '800px') %>% withSpinner(),
                             ),
                             tabPanel(
                                 'Pseudobulk gene expression',
                                 br(),
                                 plotlyOutput('out.ssp.exprs.pseudobulk',
                                              height = '500px',
                                              width = '800px') %>% withSpinner()
                             )
                         )
                         # overview UMAP
                         
                     ) # end mainPalen
                 ) # end sidebarLayout
        ), # end single species
        
        
        ### compare the three species --------------------------------------------
        tabPanel(title = 'Comparative analysis across species',
                 br(),
                 sidebarLayout(
                     sidebarPanel(
                         p('Expression comparison of three-way 1:1 orthologs.'),
                         selectInput('ms.gene.user.species',
                                     'Species for gene selection',
                                     choices = c(
                                         'Human',
                                         'Mouse',
                                         'Opossum'
                                     )),
                         textInput('ms.gene.user.input',
                                   'Enter a gene [three way orthologs only]',
                                   value = 'Ca8'),
                         submitButton('Apply'),
                         br(),
                         p('CPM values calculated based on exonic UMI counting mode.'),
                         width = 3
                     ), # end sidebarPanel
                     
                     mainPanel(
                         br(),
                         plotlyOutput('ms.pb.exprs', width = '900px', height = '400px')
                     ) # end mainPanel
                 )
                 
        ), # end comparative stuff
        
        ### FAQ ------------------------------------------------------------------
        tabPanel(
            'Info, Methodology & Data',
            fluidPage(
                fluidRow(
                    column(
                        8,
                        h3('Reference'),
                        em("Sepp, Mari; Leiss, Kevin; Sarropoulos, Ioannis; Murat, 
                          Florent; Okonechnikov, Konstantin; Joshi, Piyush; Leushkin, 
                          Evgeny; Mbengue, Noe; Schneider, Céline; Schmidt, Julia; Trost, 
                          Nils; Spänig, Lisa; Giere, Peter; Khaitovich, Philipp; Lisgo, 
                          Steven; Palkovits, Miklós; Kutscher, Lena M.; Anders, Simon; 
                          Cardoso-Moreira, Margarida; Pfister, Stefan M.; Kaessmann, Henrik, 2021, ",
                          strong("Cellular development and evolution of the mammalian cerebellum [submitted]")),
                        br(),
                        br(),
                        p('Please see the corresponding ', a('app', href = 'http://brain-match.org/'),
                          'by the ', 
                          a('Pfister lab (DKFZ, KiTZ)', href = 'https://www.kitz-heidelberg.de/en/the-kitz/kitz-directorate/prof-dr-med-stefan-pfister/'),
                          ' for further downstream analysis of the human data
                          in medulloblastoma context.'),
                        h3('Methodology'),
                        p("Data was generated from isolated single nuclei of snap-frozen cerebellum tissue samples from
                           human, mouse and opossum.
                           We used 10x 3' chromium protocol (version 2 and 3) to capture the transcriptome. For further
                           details, please see the referenced article."),
                        h3('Data & code access'),
                        p('You can download ', em('SingleCellExperiment'), ' objects using the links below.
                           The data is fully annotated and contains the UMAP and LIGER embeddings.'),
                        fluidRow(
                            column(4,
                                   strong('Human'),
                                   br(),
                                   a('pre-mRNA', href = 'sce_intronic/hum_sce.rds'),
                                   ' | ',
                                   a('exonic', href = 'sce_exonic/hum_sce.rds')
                                   ),
                            column(4,
                                   strong('Mouse'),
                                   br(),
                                   a('pre-mRNA', href = 'sce_intronic/mou_sce.rds'),
                                   ' | ',
                                   a('exonic', href = 'sce_exonic/mou_sce.rds')
                                   ),
                            column(4,
                                   strong('Opossum'),
                                   br(),
                                   a('pre-mRNA', href = 'sce_intronic/opo_sce.rds'),
                                   ' | ',
                                   a('exonic', href = 'sce_exonic/opo_sce.rds')
                                   ),
                        ),
                        br(),
                        p("Raw sequencing data is available at ",
                          a("heiDATA", href = "https://doi.org/10.11588/data/QDOC4E"), "."),
                        p('The source code of this app can be found ', 
                          a('here', href='https://gitlab.com/kleiss/shiny-mammalian-cerebellum'), "."),
                        h3('Contact'),
                        p('If you have issues or find bugs, please report them via ', 
                          a('mail', href = 'mailto:k.leiss@zmbh.uni-heidelberg.de'),
                          ' or via gitlab.'),
                        p('Furthermore, feel free to contact us via mail: ',
                          a('Mari Sepp', href = 'mailto:m.sepp@zmbh.uni-heidelberg.de'), ', ',
                          a('Kevin Leiss', href = 'mailto:k.leiss@zmbh.uni-heidelberg.de'), ', ',
                          a('Henrik Kaessmann', href = 'mailto:h.kaessmann@zmbh.uni-heidelberg.de')),
                        h3('Data Privacy'),
                        p(
                          a(
                            'Data Privacy Notice | Datenschutzerklärung',
                            href = 'https://privacy.kaessmannlab.org/sc-cerebellum-transcriptome.html'
                          )
                        ),
                        offset = 2)
                    )
                )
            # )
        ) # end of FAQ
    ) # end tabsetPanel
) # end fluidpage


# SERVER -----------------------------------------------------------------------
# Define server logic required to draw a histogram
server <- function(input, output) {
    
    # TAB: single species ------------------------------------------------------
    single.species.sce <- reactive({
        return( scl[[input$in.species.to.use]][[input$in.count.to.use]] )
    })
    
    single.species.umap <- reactive({
        sce <- scl[[input$in.species.to.use]][["pre-mRNA"]]
        um <- reducedDim(sce, 'umap2d') %>% 
            as.data.frame() %>% 
            rownames_to_column('cell_id') %>% 
            as_tibble(.)
        colnames(um) <- c('cell_id',
                          'UMAP1',
                          'UMAP2')
        return(um)
    })
    
    single.species.gene.id <- reactive({
        if(!is.null(input$in.single.gene)){
            
            gn <- input$in.single.gene
            GN <- toupper(gn)
            
            # if the ENSEBL ID is given:
            if(str_starts(GN, 'ENS')){
                return(GN)
            }else if(GN == ''){
                return(NULL)
            }else{
                useable.genes <- rownames(single.species.sce()) %>% 
                    toupper()
                
                useable.gene.names <- toupper(rowData(single.species.sce())[,"gene_name"])
                
                if(GN %in% useable.gene.names){
                    return(useable.genes[which(useable.gene.names %in% GN)])
                }else{
                    return(NULL)
                }
                
            }
            
        }
    })
    
    output$in.single.gene.ui <- renderUI({
        if(input$in.color.by == 'exprs'){
            textInput('in.single.gene',
                      label = 'Gene to plot [ENSEMB ID, symbol]',
                      placeholder = 'Pax2')
        }else{
            return(NULL)
        }
    })
    
    output$in.sinlge.gene.log.ui <- renderUI({
        if(input$in.color.by == 'exprs'){
            checkboxInput('in.single.gene.log',label = 'Expression log scale', value = FALSE)
        }
    })
    
    sp.md <- reactive({
        um <- single.species.umap()
        
        md <- colData(single.species.sce()) %>% 
            as.data.frame() %>% 
            rownames_to_column('cell_id') %>% 
            as_tibble() %>% 
            left_join(um, by = 'cell_id') %>% 
            mutate(st.ord = as.numeric(as.factor(stage.ord))) 
        
        
        md <- md %>% 
            unite("Stage", species, Stage, sep = '_') %>% 
            mutate(Stage = fct_reorder(Stage, st.ord))
        
        if(!is.null(single.species.gene.id())){
            use_gene <- single.species.gene.id()
            
            print(use_gene)
            
            exprs <- tibble(
                cell_id = colnames(single.species.sce()),
                umi = assay(single.species.sce(), 'umi')[use_gene, ],
                exprs_vec = assay(single.species.sce(), 'umi')[use_gene, ] / 
                    colData(single.species.sce())[['size_factor']] * 1e6)
            
            print(exprs)
            
            md <- md %>% 
                left_join(exprs)
            
            if(!is.null(input$in.single.gene.log)){
                if(input$in.single.gene.log){
                    md$exprs_vec <- log10(md$exprs_vec + 1)
                } 
            }
        }
        
        md <- md %>% 
            sample_n(n())
        
        return(md)
    })
    
    ## UMAP --------------------------------------------------------------------
    output$out.ssp.umap.overview <- renderPlotly({
        
        md <- sp.md()
        
        if(input$in.color.by != 'exprs'){
            plt <- md %>% 
                ggplot(aes(UMAP1, 
                           UMAP2, 
                           color = get(input$in.color.by),
                           text = get(input$in.color.by))) +
                geom_point(size = 0.05) +
                coord_fixed()
            
            if(input$in.color.by %in% names(color.list)){
                plt <- plt +
                    scale_color_manual(values = color.list[[input$in.color.by]],
                                       na.value = 'gray',
                                       labels = function(x) str_remove(x, '^HUM_|^MOU_|^OPO_'))
            }
            
            
            plt <- plt +
                labs(
                    color = names(umap.color.by.choices)[umap.color.by.choices == input$in.color.by]
                )
            
        }else{
            shiny::validate(
                need('exprs_vec' %in% colnames(md), 'Gene not found in the dataset')
            )
            plt <- md %>% 
                ggplot(aes(UMAP1, 
                           UMAP2, 
                           color = exprs_vec,
                           text = cell_type)) +
                geom_point(size = 0.05) +
                scale_color_gradientn(colours = rev(viridis::inferno(100))) +
                coord_fixed() +
                labs(color = 'CPM')
            
            # if(!is.null(input$in.single.gene.log)){
            #     if(input$in.single.gene.log){
            #         plt <- plt +
            #             labs(color = 'log10(CPM + 1)')
            #     }else{
            #         plt <- plt +
            #             labs(color = 'CPM')
            #     }
            # }
            
            plt <- plt + 
                ggtitle(paste0(input$in.single.gene, " [", single.species.gene.id(),"]"))
        }
        
        
        plt %>% 
            ggplotly(tooltip = 'text',
                     dynamicTicks = TRUE) %>% 
            toWebGL() 
    })
    
    ## pseudobulk expression ----------------------------------------------------
    output$out.ssp.exprs.pseudobulk <- renderPlotly({
        if(is.null(input$in.single.gene)){
            return(NULL)
        }else{
            if(input$in.single.gene == ''){
                return(NULL)
            }else{
                validate(
                    need('umi' %in% colnames(sp.md()), 'Gene not found in the dataset')
                )
                md <- sp.md()
                
                md <- md %>% 
                    filter(!is.na(cell_type)) %>% 
                    group_by(Stage, TissueID, Capture.System, cell_type) %>% 
                    filter(n() > 50) %>% 
                    summarise(umi = sum(umi),
                              sf = sum(size_factor)) %>% 
                    mutate(exprs = umi / sf * 1e6)
                
                plt <- md %>% 
                    ungroup() %>% 
                    mutate(stage_n = as.numeric(Stage)) %>% 
                    mutate(Stage = str_remove(Stage, '^..._')) %>% 
                    ggplot(aes(x = stage_n, 
                               y = exprs, 
                               stage = Stage,
                               color = cell_type,
                               celltype = cell_type)) +
                    stat_summary(aes(group = cell_type),
                                 fun = 'median',
                                 fun.max = max,
                                 fun.min = min) +
                    stat_summary(aes(group = cell_type),
                                 geom = 'line',
                                 fun = 'median') +
                    scale_x_continuous(breaks = 1:length(unique(md$Stage)),
                                       labels = function(x) levels(md$Stage)[x]) +
                    scale_color_manual(values = color.list[['cell_type']]) +
                    theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1)) +
                    labs(
                        x = 'developmental stage',
                        y = 'CPM',
                        color = 'Cell type',
                        title = paste0(input$in.single.gene, " [", single.species.gene.id(),"]"),
                        subtitle = 'Pseudobulk-based gene expression along development'
                    )
                
                plt <- plt %>% 
                    ggplotly(tooltip = c('celltype', 'Stage'))
                
                plt <- plt %>% 
                    layout(xaxis = list(
                        dtick = 1
                    ))
                
                return(plt)
            }
        }
    })
    
    
    
    # TAB: comparative --------------------------------------------------------
    
    # identify the user specified gene
    ms.gene.id <- reactive({
        g.user <- input$ms.gene.user.input
        G.user <- toupper(g.user)
        
        if(input$ms.gene.user.species == 'Human'){
            if(str_starts(G.user, 'ENS')){
                tmp <- o2o.info %>% 
                    filter(`Human gene stable ID` == G.user)
                
                if(nrow(tmp) == 0){
                    gid <- NULL
                }else(
                    gid <- tmp %>% pull(`Mouse gene stable ID`)
                )
            }
            
            tmp <- o2o.info %>% 
                filter(`Human gene name` == G.user) %>% 
                pull(`Mouse gene stable ID`)
            
            if(length(tmp) == 0) gid <- NULL
            
            gid <- tmp
        }else if(input$ms.gene.user.species == 'Mouse'){
            if(str_starts(G.user, 'ENS')){
                tmp <- o2o.info %>% 
                    filter(`Mouse gene stable ID` == G.user)
                
                if(nrow(tmp) == 0){
                    gid <- NULL
                }else(
                    gid <- tmp %>% pull(`Mouse gene stable ID`)
                )
            }
            
            tmp <- o2o.info %>% 
                filter(toupper(`Mouse gene name`) == G.user) %>% 
                pull(`Mouse gene stable ID`)
            
            if(length(tmp) == 0) gid <- NULL
            
            gid <- tmp
        }else if(input$ms.gene.user.species == 'Opossum'){
            if(str_starts(G.user, 'ENS')){
                tmp <- o2o.info %>% 
                    filter(`Opossum gene stable ID` == G.user)
                
                if(nrow(tmp) == 0){
                    gid <- NULL
                }else(
                    gid <- tmp %>% pull(`Opossum gene stable ID`)
                )
            }
            
            tmp <- o2o.info %>% 
                filter(toupper(`Opossum gene name`) == G.user) %>% 
                pull(`Mouse gene stable ID`)
            
            if(length(tmp) == 0) gid <- NULL
            
            gid <- tmp
        }
        
        return(gid)
    })
    
    output$ms.pb.exprs <- renderPlotly({
        # if(!ms.gene.id() %in% o2o.pb.exprs$gene_id) return(NULL)
        validate(
            need(!is.na(ms.gene.id()), 'Gene not found in the dataset'),
            need(ms.gene.id() %in% o2o.pb.exprs$gene_id, 'Gene not found in the dataset')
        )
        
        plt <- o2o.pb.exprs %>% 
            filter(gene_id == ms.gene.id()) %>% 
            ggplot(aes(x = stage,
                       y = value,
                       celltype = cell_type,
                       color = cell_type)) +
            stat_summary(aes(group = cell_type),
                         fun = median,
                         fun.max = max,
                         fun.min = min) +
            stat_summary(aes(group = cell_type),
                         fun = median,
                         geom = 'line') +
            facet_grid(cols = vars(species)) +
            scale_color_manual(values = color.list$cell_type) +
            theme(axis.text.x = element_text(angle = 45, 
                                             hjust = 1,
                                             vjust = 1)) +
            labs(
                x = '\nmatched stage',
                y = 'CPM\n',
                color = 'Cell type'
            )
        
        return(
            plt %>% 
                ggplotly(tooltip = c('celltype', 'x')) %>% 
                layout_ggplotly()
        )
    })
    
    

}

# Run the application 
shinyApp(ui = ui, server = server)
