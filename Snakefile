species = ['hum', 'mou', 'opo']

rule parse_intronic_sce:
    input:
        rds = "/home/kleiss/ws/cerebellum_analysis/data/sce/updated/{species}_sce.rds",
        script = "scripts/prepare_single_nuc_data.R"
    output: "data/sce_intronic/{species}_sce.rds"
    threads: 1
    shell:
        '''
        Rscript --vanilla \
            scripts/prepare_single_nuc_data.R \
            {input.rds} {output}
        '''

rule parse_exonic_sce:
    input:
        rds = "/home/kleiss/ws/cerebellum_analysis/data/sce/updated_exonic/{species}_exonic_sce.rds",
        script = "scripts/prepare_single_nuc_data.R"
    output: "data/sce_exonic/{species}_sce.rds"
    threads: 1
    shell:
        '''
        Rscript --vanilla \
            scripts/prepare_single_nuc_data.R \
            {input.rds} {output}
        '''

rule all_sce:
    input:
        expand("data/sce_exonic/{species}_sce.rds", species = species),
        expand("data/sce_intronic/{species}_sce.rds", species = species)
